module main
import os;
import io;

struct DataValue
{
	mut:
		a string
		b int
}

fn main() {
	mut db := AironeDb.new[DataValue]()!;
	defer
	{
		db.free();
	}
	println(db.count());
	db.push(
		DataValue{
			a: "test1",
			b: 45
		}
	)!;
	db.set_field(0, "a", "ciao")!;
}

pub struct AironeDb[T]
{
	mut:
		filewriter os.File
		elements []T
}
pub fn AironeDb.new[T]() !AironeDb[T]
{
	return AironeDb.new_with_filename[T](@STRUCT)
}
pub fn AironeDb.new_with_filename[T](filename string) !AironeDb[T]
{
	mut tmp_array := []T{};
	
	// Leggi dati vecchi
	// DUMPFILE
	filename_dump := "${filename}.csv";
	filename_changes := "${filename}.changes.csv";
	if os.is_readable(filename_dump)
	{
		mut f := os.open_file(filename_dump, "r")!;
		mut reader := io.new_buffered_reader(
			io.BufferedReaderConfig{reader: f}
		);
		{
			header_line := reader.read_line()!.split("\t");
			mut i:=0;
			$for field in T.fields
			{
				if field.name != header_line[i]
				{
					return error("Mismatch between struct fields and csv column names")
				}
				i++;
			}
		}
		for
		{
			line := reader.read_line() or { break }
			line_pieces := line.split("\t");
			mut result := T{}
			mut i := 0;
			$for field in T.fields
			{
				$if field.typ is string
				{
					result.$(field.name) = str_to_value[string](
						line_pieces[i]
					);
				}
				$else $if field.typ is int
				{
					result.$(field.name) = line_pieces[i].int()
				}
				i+=1;
			}
			tmp_array << result
		}
		f.close();
	}

	// Modifiche incrementali
	if os.is_readable(filename_changes)
	{
		mut f := os.open_file(filename_changes, "r")!;
		defer
		{
			f.close();
		}
		mut reader := io.new_buffered_reader(
			io.BufferedReaderConfig{reader: f}
		);

		for
		{
			line := reader.read_line() or { break }
			line_pieces := line.split("\t");
			match line_pieces[0]
			{
				"A"
				{
					index := line_pieces[1].int();
					mut result := T{}
					mut i := 2;
					$for field in T.fields
					{
						$if field.typ is string
						{
							result.$(field.name) = str_to_value[string](
								line_pieces[i]
							);
						}
						$else $if field.typ is int
						{
							result.$(field.name) = line_pieces[i].int()
						}
						i+=1;
					}
					tmp_array.insert(index, result);
				}
				"E"
				{
					index := line_pieces[1].int();
					field_name := line_pieces[2];
					field_value_str := line_pieces[3];
					// Check field exists
					{
						mut struct_fields := []string{};
						$for field in T.fields
						{
							struct_fields << field.name;
						}
						if field_name !in struct_fields
						{
							return error("Mismatch between edit operation column name \"${field_name}\" and struct fields")
						}
					}

					// Assign
					$for field in T.fields
					{
						if field.name == field_name
						{
							$if field.typ is string
							{
								tmp_array[index].$(field.name) = str_to_value[string](
									field_value_str
								);
							}
							$else $if field.typ is int
							{
								tmp_array[index].$(field.name) = field_value_str.int()
							}
						}
					}
				}
				"D"
				{
					index := line_pieces[1].int();
					tmp_array.delete(index);
				}
				else
				{
					return error("formato errato file modifiche incrementali")
				}
			}			
		}
		f.close();
	}

	// Write back compacted data
	{
		mut data_str := ""
		$for field in T.fields
		{
			data_str += "${field.name}\t";
		}
		data_str = data_str.substr_ni(0, -1);
		data_str += "\n";

		for v in tmp_array
		{
			data_str += serialize(v) + "\n";
		}
		os.write_file(filename_dump, data_str)!;

		os.write_file(filename_changes, "")!;
	}

	return AironeDb[T]
	{
		filewriter: os.open_append(filename_changes)!,
		elements: tmp_array
	}
}
pub fn (self AironeDb[T]) count[T]() int
{
	return self.elements.len
}
pub fn (mut self AironeDb[T]) push[T](new_value T) !
{
	self.insert(self.elements.len, new_value)!;
}
pub fn (mut self AironeDb[T]) insert[T](index int, new_value T) !
{
	self.elements.insert(index, new_value);
	self.filewriter.write_string(
		"A\t${index}\t${serialize(new_value)}\n"
	)!;
}
pub fn (mut self AironeDb[T]) remove[T](index int) !
{
	self.elements.delete(index);
	self.filewriter.write_string(
		"D\t${index}\n"
	)!;
}
pub fn (mut self AironeDb[T]) set_field[T, V](index int, field_name string, new_value V)!
{
	mut found := false;
	$for field in T.fields
	{
		$if field.typ is V
		{
			if field.name == field_name
			{
				self.elements[index].$(field.name) = new_value;
				self.filewriter.write_string(
					"E\t${index}\t${field_name}\t${new_value}\n"
				)!
				found = true;
			}
		}
	}
	if !found
	{
		return error("Trying to set \"${field_name}\" field, but it doesn't exist")
	}
}

pub fn (mut self AironeDb[T]) free[T]()
{
	self.filewriter.close();
}


fn serialize[T](a T) string
{
	mut st := "";
	$for field in T.fields
	{
		v:=a.$(field.name);
		st += "${v}\t";
	}
	if st.len > 0
	{
		st=st.substr_ni(0, -1);
	}
	return st
}

fn value_to_str[V](v V) string
{
	$if v is string
	{
		return v.replace("\\t", "\t").replace("\\n", "\n")
	}
	$else
	{
		return "${v}"
	}
}

fn str_to_value[V](v string) string
{
	return v.replace("\t", "\\t").replace("\n", "\\n")
}