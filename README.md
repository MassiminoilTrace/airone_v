This is an **experimental** port of [airone](https://gitlab.com/MassiminoilTrace/airone) to the [V programming language](vlang.io) to test its compile time reflection features.
